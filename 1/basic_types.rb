# frozen_string_literal: true

my_num = 25

my_boolean = true

my_string = "Ruby"

puts my_num
puts my_boolean
puts my_string
print "on rails!\n"

puts "I love espresso".length

puts "I love espresso".reverse

puts "I love espresso".upcase

puts "I love espresso".upcase
                      .downcase