# frozen_string_literal: true

puts "What's your name?"
variable_name = gets.chomp
variable_name.capitalize!
puts "What's your last name?"
last_name = gets.chomp.capitalize
last_name.capitalize!
puts "What's your city?"
city = gets.chomp.capitalize
city.capitalize!
puts "What's your state?"
state = gets.chomp
state.upcase!

print variable_name
print last_name
print city
print state
puts "You are #{variable_name} #{last_name} and live in #{city}, #{state}"

